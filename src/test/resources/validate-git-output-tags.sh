#!/bin/bash

#-----------------------------------------------------------------------------
# Validate git output for all versions of git
#
#-----------------------------------------------------------------------------

#set -x

#-----
# Globals
#-----


#-----
# Functions
#-----



#-----
# M A I N
#-----

test_dir='/d/temp/val-git-output'
git_install_dir='/e/Install/Git/bin'

read -r -d '' ref_output_test1 <<'END'
commit 0000000000000000000000000000000000000000
Author: Commit User <cu@example.com>
Date:   Tue Jan 24 00:00:00 2017 +0100

    Commit with leightweight-tag-1
-----
commit 0000000000000000000000000000000000000000
Author: Commit User <cu@example.com>
Date:   Tue Jan 24 00:00:00 2017 +0100

    Commit with leightweight-tag-2 and leightweight-tag-3
-----
commit 0000000000000000000000000000000000000000
Author: Commit User <cu@example.com>
Date:   Tue Jan 24 00:00:00 2017 +0100

    Commit with leightweight-tag-2 and leightweight-tag-3
-----
tag annotated-tag-1
Tagger: Tag User <tu@example.com>
Date:   Tue Jan 24 00:00:00 2017 +0100

This is an annotated tag

commit 0000000000000000000000000000000000000000
Author: Commit User <cu@example.com>
Date:   Tue Jan 24 00:00:00 2017 +0100

    Commit with annotated-tag-1
END

read -r -d '' ref_output_test2 <<'END'
0000000000000000000000000000000000000000:: (HEAD -> master, tag: leightweight-tag-3, tag: leightweight-tag-2)
0000000000000000000000000000000000000000:: (tag: annotated-tag-1)
0000000000000000000000000000000000000000:: (tag: leightweight-tag-1)
0000000000000000000000000000000000000000::
END


rm -rf "$test_dir"
mkdir -p "$test_dir"
cd "$test_dir"

echo "$ref_output_test1" > "$test_dir/ref_output_test1.txt"
echo "$ref_output_test2" > "$test_dir/ref_output_test2.txt"

mkdir repo
cd repo
git init

git config user.name 'Tag User'
git config user.email 'tu@example.com'

git commit -m "Commit without tag" --allow-empty --author="Commit User <cu@example.com>"
git commit -m "Commit with leightweight-tag-1" --allow-empty --author="Commit User <cu@example.com>"
git tag leightweight-tag-1
git commit -m "Commit with annotated-tag-1" --allow-empty --author="Commit User <cu@example.com>"
git tag -a annotated-tag-1 -m "This is an annotated tag"
git commit -m "Commit with leightweight-tag-2 and leightweight-tag-3" --allow-empty --author="Commit User <cu@example.com>"
git tag leightweight-tag-2
git tag leightweight-tag-3

#
# Test git notes
#
git commit --allow-empty -m "Commit 1 with note"
git note add -m "Note on commit 1"


for dir in /e/Install/Git/bin/*/
do
    git_version=${dir%*/}
    git_version=${git_version##*/}
    git_command="${dir}bin/git"
    echo "Testing git output with git version (dir): $git_version"
    echo "    Binary:            $git_command"
    echo "    Detailed version:  `$git_command --version`"

    #eval "$git_command --version"

    #
    # Test git show -s
    #
    output_filename="$test_dir/output-test1-${git_version}.txt"
    eval "$git_command show -s leightweight-tag-1 > $output_filename"
    echo "-----" >> $output_filename
    eval "$git_command show -s leightweight-tag-2 >> $output_filename"
    echo "-----" >> $output_filename
    eval "$git_command show -s leightweight-tag-3 >> $output_filename"
    echo "-----" >> $output_filename
    eval "$git_command show -s annotated-tag-1 >> $output_filename"

    # Normalize date and commit id
    sed -i -E 's/^Date:   .+/Date:   Tue Jan 24 00:00:00 2017 +0100/' "$output_filename"
    sed -i -E 's/^commit .+/commit 0000000000000000000000000000000000000000/' "$output_filename"

    diff "$test_dir/ref_output_test1.txt" "$output_filename" > /dev/null
    if [[ $? != 0 ]]; then
        echo " Test 1: D I F F  with git version $git_version"
    fi

    #
    # Test git show --oneline --format="%H::%D"
    #
    output_filename="$test_dir/output-test2-${git_version}.txt"
    eval "$git_command show --format='%H::%d' `git log --format='%H' | tr '\n' ' '`" > $output_filename
    #eval "/e/Install/Git/bin/2.9.2/bin/git show --format='%H::%D' `git log --format='%H' | tr '\n' ' '`"

    #eval "$git_command show --format='%H::%D'"
    #git log --format='%H' | tr '\n' ' ' | xargs -I {} eval "$git_command show --format='%H::%D' " {} > $output_filename
    sed -i -E 's/^[^:]+/0000000000000000000000000000000000000000/' "$output_filename"
    diff --side-by-side --suppress-common-lines -W $(( $(tput cols) - 2 )) "$test_dir/ref_output_test2.txt" "$output_filename" #> /dev/null
    if [[ $? != 0 ]]; then
        echo " >>>> Test 2: D I F F <<<<"
    fi
    echo ""

    #
    # Test git notes
    #
    output_filename="$test_dir/output-test3-${git_version}.txt"
    eval "$git_command show --format='%H::%d' `git log --format='%H' | tr '\n' ' '`" > $output_filename

    #exit

done

echo "Done."

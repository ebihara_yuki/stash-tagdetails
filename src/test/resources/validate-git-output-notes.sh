#!/bin/bash

#-----------------------------------------------------------------------------
# Validate git output for all versions of git
#
#-----------------------------------------------------------------------------

#set -x

#-----
# Globals
#-----


#-----
# Functions
#-----



#-----
# M A I N
#-----

test_dir='/d/temp/val-git-output'
git_install_dir='/e/Install/Git/bin'

read -r -d '' ref_output_test3 <<'END'
commit 0000000000000000000000000000000000000000
Author: Commit User <cu@example.com>
Date:   Tue Jan 24 00:00:00 2017 +0100

    Commit 3 with note in default and in namespace_3

Notes (namespace_3):
    Note on commit 3 in namespace_3

commit 0000000000000000000000000000000000000000
Author: Commit User <cu@example.com>
Date:   Tue Jan 24 00:00:00 2017 +0100

    Commit 2 with note in namespace_2

Notes (namespace_2):
    Note on commit 2 in namespace_2

commit 0000000000000000000000000000000000000000
Author: Commit User <cu@example.com>
Date:   Tue Jan 24 00:00:00 2017 +0100

    Commit 1 with note

Notes:
    Note on commit 1
END



rm -rf "$test_dir"
mkdir -p "$test_dir"
cd "$test_dir"

echo "$ref_output_test3" > "$test_dir/ref_output_test3.txt"

mkdir repo
cd repo
git init

git commit --allow-empty -m "Commit 1 with note" --author="Commit User <cu@example.com>"
git notes add -m "Note on commit 1"
git commit --allow-empty -m "Commit 2 with note in namespace_2" --author="Commit User <cu@example.com>"
git notes --ref=namespace_2 add -m "Note on commit 2 in namespace_2"
git commit --allow-empty -m "Commit 3 with note in default and in namespace_3" --author="Commit User <cu@example.com>"
git notes --ref=namespace_3 add -m "Note on commit 3 in namespace_3"


for dir in /e/Install/Git/bin/*/
do
    git_version=${dir%*/}
    git_version=${git_version##*/}
    git_command="${dir}bin/git"
    echo "Testing git output with git version (dir): $git_version"
    echo "    Binary:            $git_command"
    echo "    Detailed version:  `$git_command --version`"

    #
    # Test git notes
    #
    output_filename="$test_dir/output-test3-${git_version}.txt"

    eval "$git_command show --show-notes=* -s `git log --format='%H' | tr '\n' ' '`" > $output_filename

    sed -i -E 's/^commit.+/commit 0000000000000000000000000000000000000000/' "$output_filename"
    sed -i -E 's/^Date:   .+/Date:   Tue Jan 24 00:00:00 2017 +0100/' "$output_filename"
    diff --side-by-side --suppress-common-lines -W $(( $(tput cols) - 2 )) "$test_dir/ref_output_test3.txt" "$output_filename" #> /dev/null
    if [[ $? != 0 ]]; then
        echo " >>>> Test 3: D I F F <<<<"
    fi
    echo ""

    #exit

done

echo "Done."

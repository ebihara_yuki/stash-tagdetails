package jh.atlassian.bitbucket.tagdetails;

import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.Watchdog;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TagDetailsCommandOutputHandler implements CommandOutputHandler<TagDetails> {
    private static final Logger log = LoggerFactory.getLogger(TagDetailsCommandOutputHandler.class);
    TagDetails tagDetails;

    @Override
    public void process(InputStream output) throws ProcessException {
        String s = null;
        try {
            s = IOUtils.toString(output, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            s = e.getMessage();
        }

        boolean isAnnotatedTag;
        String tagName = null;
        String commitId = null;
        String tagger = null;
        String date = null;
        String message = null;
        String signature = null;

        if (s.startsWith("commit")) {
            // Its a leightweight tag
            isAnnotatedTag = false;
        } else {
            // Its an annotated tag.
            // The regex for the signature should be ok, https://tools.ietf.org/html/rfc4880.
            isAnnotatedTag = true;
            Pattern pattern = Pattern.compile("tag\\s+(.+?)$\\s*"
                    + "^Tagger:\\s+(.+?)$\\s*"
                    + "^Date:\\s+(.+?)$\\s*"
                    + "(.+?)$\\s*"
                    + "(^-----BEGIN PGP SIGNATURE-----$\\s*(.+?)\\s*^-----END PGP SIGNATURE-----$\\s*)?"
                    + "^commit\\s([0-9a-f]{40})$\\s*"
                    + "(^Merge:\\s+(.+?)$\\s*)?"
                    + "^Author:\\s+(.+?)$\\s*"
                    + "^Date:\\s+.*", Pattern.MULTILINE | Pattern.DOTALL);
            Matcher matcher = pattern.matcher(s);
            if (matcher.matches()) {
                tagName = matcher.group(1);
                tagger = matcher.group(2);
                date = matcher.group(3);
                message = matcher.group(4);
                signature = matcher.group(6);
                commitId = matcher.group(7);
            }
        }

        tagDetails = new TagDetails(tagName, commitId, isAnnotatedTag, tagger, date, message, signature);
    }

    @Override
    public void complete() throws ProcessException {
        // TODO Auto-generated method stub
    }

    @Override
    public void setWatchdog(Watchdog watchdog) {
        // TODO Auto-generated method stub

    }

    @Override
    public TagDetails getOutput() {
        return tagDetails;
    }

}

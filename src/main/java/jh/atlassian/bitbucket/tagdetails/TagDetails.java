package jh.atlassian.bitbucket.tagdetails;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class TagDetails {
    @JsonProperty
    private String tagname;
    @JsonProperty
    private String tagger;
    @JsonProperty
    private String date;
    @JsonProperty
    private String message;
    @JsonProperty
    private String signature;
    @JsonProperty
    private String commit;
    @JsonProperty
    private Boolean isAnnotatedTag;

    // This class is intended for replying to GET as well as to POST. For POST, there is the need of a default constructor to avoid:
    // ThrowableExceptionMapper Uncaught exception thrown by REST service: No suitable constructor found for type
    // [simple type, class jh.atlassian.stash.tagdetails.TagDetails]: can not instantiate from JSON object (need to add/enable type information?)
    public TagDetails() {

    }

    public TagDetails(String tagname, String commit, Boolean isAnnotatedTag, String tagger, String date, String message, String signature) {
        this.tagname = tagname;
        this.commit = commit;
        this.isAnnotatedTag = isAnnotatedTag;
        this.tagger = tagger;
        this.date = date;
        this.message = message;
        this.signature = signature;
    }

    public String getTagname() {
        return tagname;
    }

    public void setTagname(String tagname) {
        this.tagname = tagname;
    }

    public String getCommit() {
        return commit;
    }

    public void setCommit(String commit) {
        this.commit = commit;
    }

    public boolean isAnnotatedTag() {
        return isAnnotatedTag;
    }

    public String getTagger() {
        return tagger;
    }

    public String getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }

    public String getSignature() {
        return signature;
    }

    @Override
    public String toString() {
        return "tag " + tagname + ", commit " + commit + ", isAnnotatedTag " + isAnnotatedTag + ", tagger " + tagger + ", date " + date + ", message "
                + message.substring(0, message.length() < 20 ? message.length() : 20);
    }

}

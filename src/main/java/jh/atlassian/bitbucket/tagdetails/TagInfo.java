package jh.atlassian.bitbucket.tagdetails;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class TagInfo {
    @JsonProperty
    TagDetails tagDetails;
    @JsonProperty
    String errorMessage;
}

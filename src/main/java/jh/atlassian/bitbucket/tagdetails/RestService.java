package jh.atlassian.bitbucket.tagdetails;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.scm.git.command.GitScmCommandBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

/**
 */
@Path("projects/{projectKey}/repos/{repositorySlug}")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class RestService {
    private static final Logger log = LoggerFactory.getLogger(RestService.class);
    private final GitCommandBuilderFactory gitCommandBuilderFactory;

    public RestService(GitCommandBuilderFactory gitCommandBuilderFactory) {
        this.gitCommandBuilderFactory = gitCommandBuilderFactory;
    }

    @GET
    @Path("/tags")
    public Response getTagDetailsByTagNames(@Context HttpServletRequest request, @Context Repository repository,
                                            @QueryParam(value = "name") final List<String> tagNames) {
        Map<String, TagInfo> tagNameToTagInfoMap = new HashMap<>();
        List<TagInfo> tagInfoList = getTagInfos(repository, tagNames);
        for (TagInfo tagInfo : tagInfoList) {
            tagNameToTagInfoMap.put(tagInfo.tagDetails.getTagname(), tagInfo);
        }
        // Always OK, even if one (or at least all) tags not present. Error info is given for each tag.
        Response response = Response.status(Response.Status.OK).entity(tagNameToTagInfoMap).build();
        return response;
    }

    private List<TagInfo> getTagInfos(Repository repository, List<String> tagNames) {
        GitScmCommandBuilder builder = gitCommandBuilderFactory.builder(repository);
        GitShowCommandExitHandler exitHandler = new GitShowCommandExitHandler();
        // tagNames: Values are URL decoded by @QueryParam
        List<TagInfo> tagInfoList = new ArrayList<>();
        // tagNames: Values are URL decoded by @QueryParam
        for (String tagName : tagNames) {
            // https://answers.atlassian.com/questions/11974547/git-shortlog
            TagDetailsCommandOutputHandler outputHandler = new TagDetailsCommandOutputHandler();
            builder.clearArguments();
            TagInfo tagInfo = new TagInfo();
            // -s: Suppress diff output.
            tagInfo.tagDetails = builder.command("show").argument(tagName).argument("-s").exitHandler(exitHandler).build(outputHandler).call();
            // In case of leightweight tag, the output handler has no information about the tag and therefore can't set set tag name. Set it afterwards here.
            tagInfo.tagDetails.setTagname(tagName);
            if (exitHandler.getExitCode() != 0) {
                tagInfo.errorMessage = exitHandler.getErrorMessage();
            }
            tagInfoList.add(tagInfo);
        }
        return tagInfoList;
    }

    @POST
    @Path("/notes")
    public Response getNotesByCommitIds(@Context HttpServletRequest request, @Context Repository repository, Set<String> commitIds) {
        //log.warn("getNotesByCommitIds(), commitIds {}", commitIds);
        GitScmCommandBuilder builder = gitCommandBuilderFactory.builder(repository);
        GitShowCommandExitHandler exitHandler = new GitShowCommandExitHandler();
        NotesCommandOutputHandler outputHandler = new NotesCommandOutputHandler();
        // -s: Suppress diff output.
        builder.command("show").argument("--show-notes=*").argument("-s");
        for (String commitId : commitIds) {
            builder.argument(commitId);
        }
        List<NotesModel> notesModels = builder.exitHandler(exitHandler).build(outputHandler).call();
        Response response = Response.status(Response.Status.OK).entity(notesModels).build();
        return response;
    }
}

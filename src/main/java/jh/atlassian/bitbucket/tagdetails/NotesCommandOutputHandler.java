package jh.atlassian.bitbucket.tagdetails;

import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.Watchdog;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class NotesCommandOutputHandler implements CommandOutputHandler<List<NotesModel>> {
    private static final Logger log = LoggerFactory.getLogger(NotesCommandOutputHandler.class);
    private static final String COMMITS_DELIMITER_REGEX = "(?m)^commit\\s+";
    private List<NotesModel> notes = new ArrayList<>();

    @Override
    public void process(InputStream is) throws ProcessException {
        try {
            String out = IOUtils.toString(is, "UTF-8");
            String[] commits = out.split(COMMITS_DELIMITER_REGEX);
            for (String commit : commits) {
                if (commit.isEmpty()) {
                    continue;   // Skip first array index, it is empty
                }
                String commitId = commit.substring(0, 40);
                String[] notesMessages = commit.split("(?m)^Notes");
                NotesModel notesModel = new NotesModel(commitId);
                for (int notesMessageIndex = 1; notesMessageIndex < notesMessages.length; notesMessageIndex++) {
                    String namespace = "";
                    StringBuilder sb = new StringBuilder();
                    String[] lines = StringUtils.splitPreserveAllTokens(notesMessages[notesMessageIndex], System.lineSeparator());
                    for (int lineIndex = 0; lineIndex < lines.length; lineIndex++) {
                        if (lineIndex == 0) {
                            // Notes-tokens are:
                            //  'Notes:' for default namespace
                            //  'Notes (namespace_1):' for a namespace namespace_1
                            namespace = "Notes" + lines[lineIndex];
                        } else {
                            sb.append(lines[lineIndex].replaceFirst("^\\s{4}", ""));
                            if (lineIndex < lines.length - 1) {
                                sb.append("\n");
                            }
                        }
                    }
                    notesModel.addNamespacedNotes(namespace, sb.toString().replaceFirst("(?m)[\\n\\r]*\\Z", ""));
                }
                if (notesModel.hasNotes()) {
                    notes.add(notesModel);
                }
            }
        } catch (IOException e) {
            // No more exception handling here. If IO problem, this is not critical.
            e.printStackTrace();
        }
    }

    @Override
    public void complete() throws ProcessException {
        // TODO Auto-generated method stub
    }

    @Override
    public void setWatchdog(Watchdog watchdog) {
        // TODO Auto-generated method stub
    }

    @Override
    public List<NotesModel> getOutput() {
        return notes;
    }

}

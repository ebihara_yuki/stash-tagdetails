package jh.atlassian.bitbucket.tagdetails;

import com.atlassian.bitbucket.scm.CommandExitHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GitShowCommandExitHandler implements CommandExitHandler {
    private static final Logger log = LoggerFactory.getLogger(GitShowCommandExitHandler.class);
    private Integer exitCode;
    private String errorMessage;

    @Override
    public void onCancel(String command, int exitCode, String stdErr, Throwable thrown) {
        // Do nothing
    }

    @Override
    public void onExit(String command, int exitCode, String stdErr, Throwable thrown) {
        // Successful command:
        // command: C:\Program Files (x86)\Git\bin\git.exe show tag-01 -q, exitCode: 0, stderr: "", thrown: null
        //
        // Failed command:
        // command: C:\Program Files (x86)\Git\bin\git.exe show tag-03 -q
        //
        // exitCode: 128
        //
        // stderr: fatal: ambiguous argument 'tag-03': unknown revision or path not in the working tree.
        //
        // Use '--' to separate paths from revisions, like this:
        //
        // 'git <command> [<revision>...] -- [<file>...]'
        //
        // thrown: com.atlassian.utils.process.ProcessException: Non-zero exit code: 128

        // log.warn("command {}, exitCode {}, stderr {}, thrown {}", command, exitCode, stdErr, thrown);

        this.exitCode = exitCode;
        if (exitCode != 0) {
            errorMessage = stdErr.split("[\\n\\r]+")[0]; // First line
        }
    }

    public Integer getExitCode() {
        return exitCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}

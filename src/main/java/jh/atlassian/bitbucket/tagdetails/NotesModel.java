package jh.atlassian.bitbucket.tagdetails;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonSerialize
public class NotesModel {
    @JsonProperty
    private String commitId;
    @JsonProperty
    private Map<String, String> namespaceToNotesMap = new HashMap<>();

    public NotesModel(String commitId) {
        this.commitId = commitId;
    }

    public void addNamespacedNotes(String namespace, String notes) {
        this.namespaceToNotesMap.put(namespace, notes);
    }

    public boolean hasNotes() {
        return !namespaceToNotesMap.isEmpty();
    }

    public Map<String, String> getNamespaceToNotesMap() {
        return namespaceToNotesMap;
    }
}

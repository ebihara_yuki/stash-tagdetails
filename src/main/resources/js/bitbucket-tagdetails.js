define('jh/atlassian/bitbucket/tagdetails', ['@atlassian/aui', 'lodash', 'bitbucket/util/state'], function (AJS, _, state) {

    const IS_LOGGING = false;
    const DIALOG_HIDE_DELAY_MS = 100;
    var gCurrentProjectKey;
    var gCurrentRepoSlug;
    var gHaveNotesSeen = false;
    // At "infinite scroll", continue numbering the tags- and notes-dialogs.
    var gDialogIndex = 0;

    const CREATE_PULLREQUEST_RE = new RegExp('\/projects\.+?\/commits\\?');
    const COMMITS_PAGE_RE = new RegExp('\/rest\/tags\/.+?\/tags$'); // Commits page
    const COMMIT_VIEW_PAGE_RE = new RegExp('\/rest\/branch-utils\/'); // Single commit page
    const CREATE_TAG_ON_SINGLE_COMMIT_PAGE_RE = new RegExp('\/rest\/git\/.+?\/tags$');          // Create tag on single commit page
    const DELETE_TAG_ON_SINGLE_COMMIT_PAGE_RE = new RegExp('\/rest\/git\/.+?\/tags/.+$');       // Delete tag on single commit page
    const COMMITS_TAB_ON_PULLREQUEST_PAGE_RE = new RegExp('\/pull-requests\/[0-9]+\/commits');  // Commits tab on pull request page

    const URL_REGEX_ARRAY = [
        CREATE_PULLREQUEST_RE,
        COMMITS_PAGE_RE,                        // Commits page
        COMMIT_VIEW_PAGE_RE,                    // Single commit page
        CREATE_TAG_ON_SINGLE_COMMIT_PAGE_RE,    // Create tag on single commit page
        DELETE_TAG_ON_SINGLE_COMMIT_PAGE_RE,    // Delete tag on single commit page
        COMMITS_TAB_ON_PULLREQUEST_PAGE_RE      // Commits tab on pull request page
    ];


    // Use this regex to remove the time stamp from logfile:
    // ^\d\d\d\d-\d\d-\d\d\s\d\d:\d\d:\d\d,\d\d\d
    function log(message) {
        if (IS_LOGGING) {
            AJS.log(moment().format('YYYY-MM-DD HH:mm:ss,SSS') + ' Tag Details ' + message);
        }
    }

    function buildTagdetailsRestBaseUrl() {
        return AJS.contextPath() + '/rest/tagdetails/1.0/projects/' + gCurrentProjectKey + '/repos/' + gCurrentRepoSlug;
    }

    //
    //  Handles the Notes column:
    //  This column is hidden by default. But if there is any note, the column will be shown.
    //  This observer let the Notes column be displayed after an infinite scroll update, even there isn't any note on the new page.
    //
    function registerCommitsTableMutationObserver() {
        if (window.MutationObserver) {
            var commitsTableMutationObserver;
            // Create a static commitsTableMutationObserver instance. This can be registered multiple times but gets called only once.
            commitsTableMutationObserver = new MutationObserver(function (mutations) {
                mutations.forEach(function (mutation) {
                    var $mutationTarget = $(mutation.target);
                    log('CommitsTableMutationObserver: mutation type: ' + mutation.type
                        + ', target ' + mutation.target
                        + ', attributeName: ' + mutation.attributeName
                        + ', value: ' + $mutationTarget.attr(mutation.attributeName)
                        + ', oldValue: ' + mutation.oldValue);

                    if (mutation.type === 'attributes') {    // Should always 'attributes' because of the config. But for sure.
                        if (mutation.attributeName === 'class' && $mutationTarget.hasClass('no-rows')) {
                            // User changed source or destination branch on Create Pull-Request page. Because we're still on the same page,
                            // clear gHaveNotesSeen.
                            log('  On commits-table pruned. Setting gHaveNotesSeen to false.');
                            // After migration from former JS InlineDialog to <aui-inline-dialog>, the dialog accidentally could be kept open
                            // and positioned to the top left screen position.
                            // To avoid this, remove all aui-inline-dialogs when the commits-table content changes.
                            $('.tagdetails-aui-inline-dialog').remove();
                            gHaveNotesSeen = false;
                            AJS.$('.bitbucket-tagdetails-notesColumn').hide();
                        } else if (mutation.attributeName === 'data-last-updated') {
                            // As per CSS, the notes-column is hidden by default. This is OK on the first page of a commits-list. But on
                            // subsequent pages not:
                            // Imagine there are two pages of commits. On the first page there are notes, and on the second page aren't.
                            // As per CSS, the Notes-column is hidden. But this JS makes is visible.
                            // You scroll to the second page. The notes column will be hidden because of the CSS. And this JS will not make
                            // is visible. We have to make sure that the column and the heading are shown if there is at least one note
                            // already shown, AND it must be kept visible. This is done by observing the commits-table and show() the notes
                            // column after any insertion of new commits, if they have notes. The commits-table has the attribute
                            // 'data-last-updated' which changes is value at insertion.
                            log('  On infinite scroll, gHaveNotesSeen ' + gHaveNotesSeen);
                            if (gHaveNotesSeen) {
                                // Notes-column is 'display: none' by default by css. Show it in case there have been at least one note.
                                AJS.$('.bitbucket-tagdetails-notesColumn').show();
                            }
                        }
                    }
                });
            });

            // Observe only on commits-table. On single commit page .observe() would result in error:
            // "Failed to execute 'observe' on 'MutationObserver': parameter 1 is not of type 'Node'."
            var $commitsTable = $('.commits-table');
            if ($commitsTable.length > 0) {
                // This commitsTableMutationObserver reacts multiple times on same value for data-last-updated. Don't know why.
                // attribute: data-last-updated
                var config = {
                    attributes: true,
                    // "Set to true to record the previous value of any attribute that changes when monitoring the node or nodes for
                    // attribute changes." Only for logging.
                    attributeOldValue: true
                };
                commitsTableMutationObserver.observe($commitsTable.get(0), config);
            }
        }
    }

    log('NEW PAGE');

    /*
     * React on finished calls
     *
     * Here are lists of URLs occurring during clicking around. The URLs of interest are marked with "=>"
     *
     * On Commits Page
     *
     *      /bitbucket/projects/PROJECT_1/repos/integrationtest/commits?merges=include&start=0&limit=50&contents
     *      /bitbucket/projects/PROJECT_1/repos/integrationtest/commits?until=refs%2Fheads%2Fb-anno-3&merges=include&start=0&limit=50&contents
     *      /bitbucket/rest/analytics/1.0/publish/bulk
     *      /bitbucket/rest/api/latest/inbox/pull-requests/count
     *      /bitbucket/rest/api/latest/profile/recent/repos?avatarSize=32
     *      /bitbucket/rest/build-status/latest/commits/stats
     *      /bitbucket/rest/jira-integration/latest/servers
     *      /bitbucket/rest/shortcuts/latest/shortcuts/5467926/bundled
     *      /bitbucket/rest/sync/latest/projects/PROJECT_1/repos/integrationtest?at=refs%2Fheads%2Fb-anno-3
     *      /bitbucket/rest/sync/latest/projects/PROJECT_1/repos/integrationtest?at=refs%2Fheads%2Fmaster
     *      /bitbucket/rest/tagdetails/1.0/projects/PROJECT_1/repos/integrationtest/tags?name=a%2Fb&name=a...
     *      /bitbucket/rest/tagdetails/1.0/projects/PROJECT_1/repos/integrationtest/tags?name=anno-3&_=1492025721729
     * =>   /bitbucket/rest/tags/latest/projects/PROJECT_1/repos/integrationtest/tags
     *
     *
     * On Commit View Page
     *  Load
     *
     *      /bitbucket/rest/analytics/1.0/publish/bulk
     *      /bitbucket/rest/api/latest/inbox/pull-requests/count
     *      /bitbucket/rest/api/latest/profile/recent/repos?avatarSize=32
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/integrationtest/commits/f8f321f628d7f9306a735c7b521cd0feb1900e4b/changes?since=f366abab67153550f1122336c8a609b9d84478f5&start=0&limit=1000
     *      /bitbucket/rest/branch-utils/latest/projects/PROJECT_1/repos/integrationtest/branches/info/f8f321f628d7f9306a735c7b521cd0feb1900e4b?limit=10
     *      /bitbucket/rest/build-status/latest/commits/stats/f8f321f628d7f9306a735c7b521cd0feb1900e4b?includeUnique=true
     *      /bitbucket/rest/shortcuts/latest/shortcuts/5467926/bundled
     *      /bitbucket/rest/tagdetails/1.0/projects/PROJECT_1/repos/integrationtest/tags?name=a...
     * =>   /bitbucket/rest/tags/latest/projects/PROJECT_1/repos/integrationtest/tags
     * =>   /bitbucket/projects/IT/repos/integrationtest/commits/e1c1fb09cb7e13991539940df168ae3c64f05b80   since 4.7
     *
     *  Create tag
     *
     *      /bitbucket/rest/analytics/1.0/publish/bulk
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/integrationtest/tags/t
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/integrationtest/tags/t20
     * =>   /bitbucket/rest/git/latest/projects/PROJECT_1/repos/integrationtest/tags
     *      /bitbucket/rest/tagdetails/1.0/projects/PROJECT_1/repos/integrationtest/tags?name=...
     *
     *  Delete tag
     *
     * =>    /bitbucket/rest/git/latest/projects/PROJECT_1/repos/integrationtest/tags/abc
     *
     *
     * On Pull Request Page:
     *  Overview Tab
     *
     *      /bitbucket/rest/jira-integration/latest/servers
     *      /bitbucket/rest/build-status/latest/commits/stats/eb16ff5a93d91801df88e1d73b1299f34bb1f780?includeUnique=true
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/integrationtest/pull-requests/2/activities?start=0&limit=25&avatarSize=64&markup=true
     *      /bitbucket/rest/api/latest/inbox/pull-requests/count
     *      /bitbucket/rest/api/latest/profile/recent/repos?avatarSize=32
     *      /bitbucket/rest/analytics/1.0/publish/bulk
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/integrationtest/pull-requests/2/merge
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/integrationtest/settings/pull-requests
     *      /bitbucket/rest/jira/latest/projects/PROJECT_1/repos/integrationtest/pull-requests/2/issues
     *
     *  Diff Tab
     *
     *      /bitbucket/rest/analytics/1.0/publish/bulk
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/rep_1/pull-requests/1/changes?start=0&limit=1000&changeScope=unreviewed
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/rep_1/pull-requests/1/diff/add_file/add_file.txt?avatarSize=64&markup=true&whitespace&contextLines=-1&withComments=true&autoSrcPath=undefined
     *      /bitbucket/rest/chaperone/1/chaperone/side-by-side-diff-discovery
     *      /bitbucket/rest/webResources/1.0/resources
     *
     *
     *  Commits Tab
     *
     * =>   /bitbucket/projects/PROJECT_1/repos/rep_1/pull-requests/1/commits?start=0&limit=50&contents
     *      /bitbucket/rest/analytics/1.0/publish/bulk
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/rep_1/pull-requests/1/changes?start=0&limit=1000&pullRequestId=1
     *      /bitbucket/rest/api/latest/projects/PROJECT_1/repos/rep_1/pull-requests/1/diff/add_file/add_file.txt?avatarSize=64&markup=true&whitespace&contextLines=-1&withComments=true&autoSrcPath=undefined
     *      /bitbucket/rest/build-status/latest/commits/stats
     *      /bitbucket/rest/tagdetails/1.0/projects/PROJECT_1/repos/rep_1/commits
     *
     */
    $(document).ajaxComplete(function (event, xhr, settings) {

        log('ajaxComplete url ' + settings.url);

        // Exit here if none of the regexes above match the current url
        if (!_.some(URL_REGEX_ARRAY, function (regex) {
                return settings.url.match(regex);
            })) {
            log('  skipping url');
            return;
        }

        log('  processing response');

        // Wait for rendering the response into the DOM
        setTimeout(function () {
                log('  after timeout');

                gCurrentProjectKey = state.getProject().key;
                gCurrentRepoSlug = state.getRepository().slug;
                log('gCurrentProjectKey ' + gCurrentProjectKey + ', gCurrentRepoSlug ' + gCurrentRepoSlug);

                registerCommitsTableMutationObserver();

                // In case of pull request page, loadTagObjectsByCommitIdsJson() loads like Bitbucket does with
                // '/rest/tags/latest/projects/...'.
                // This results in a response the COMMITS_PAGE_RE reacts on.
                // Because we are on the pull request page in that case, not both code paths (commits page, single commit page) must be
                // executed!
                // We have to check for both cases separate if they are true or false (but they both will never be true at the same time).
                if (settings.url.match(COMMITS_PAGE_RE)) {
                    var isCommitsPage = AJS.$('#commits-table').size() > 0;
                    if (isCommitsPage) {
                        log('On page: Commits List');
                        // Tags
                        readTagNamesFromCommitsPageResponse(xhr)
                            .then(loadTagDetailsByTagNames)
                            .then(setTagDetailsDialogsInCommitsList);
                        // Notes
                        readCommitIdsFromCommitsPageResponse(xhr, settings)
                            .then(loadNotesByCommitIdsJson)
                            .then(insertNotesContainersIntoCommitsList)
                            .then(showNotesColumn)
                            .then(setNotesDialogsInCommitsList);
                    }
                } else if (settings.url.match(COMMIT_VIEW_PAGE_RE)) {
                    log('On page: single Commit');
                    // Tags
                    readTagNamesFromCommitViewPageHtml()
                        .then(loadTagDetailsByTagNames)
                        .then(setTagDetailsDialogsOnCommitViewPage);
                    // Notes
                    loadNotesByCommitIdsJson('["' + state.getCommit().id + '"]')
                        .then(setNotesLabelOnCommitViewPage)
                        .then(setNotesDialogOnCommitViewPage);
                } else if (settings.url.match(CREATE_TAG_ON_SINGLE_COMMIT_PAGE_RE) || settings.url.match(DELETE_TAG_ON_SINGLE_COMMIT_PAGE_RE)) {
                    log('On page: Single Commit; Create Tag or Delete Tag');
                    // After the user adds or deletes a tag, the browser re-renders the tag-container. We have to wait until finished.
                    // While adding a tag seems to be fast, deleting a tag is unexpectedly slow. In my local tests, Chrome took ~180ms.
                    // TODO Could be better handled by an Observer?
                    setTimeout(function () {
                        readTagNamesFromCommitViewPageHtml()
                            .then(loadTagDetailsByTagNames)
                            .then(setTagDetailsDialogsOnCommitViewPage);
                    }, 300);
                    // No Notes handling at these events.
                } else if (settings.url.match(COMMITS_TAB_ON_PULLREQUEST_PAGE_RE)) {
                    log('On page: Pull-Request, tab Commits');

                    //
                    //  This pages requests all commits as json. In contrast to the Create Pull Request page.
                    //

                    // Tags
                    readCommitIdsFromPullRequestPageResponse(xhr, settings, 'json')
                        .then(loadTagObjectsByCommitIdsJson)
                        .then(insertTagsContainersIntoPullRequestCommitsList)
                        .then(loadTagDetailsByTagNames)
                        .then(setTagDetailsDialogsInCommitsList);
                    // Notes
                    readCommitIdsFromPullRequestPageResponse(xhr, settings, 'json')
                        .then(loadNotesByCommitIdsJson)
                        .then(insertNotesContainersIntoCommitsList)
                        .then(showNotesColumn)
                        .then(setNotesDialogsInCommitsList);
                } else if (settings.url.match(CREATE_PULLREQUEST_RE) && AJS.$('#compare-commits-table').length > 0) {
                    log('On page: Create Pull-Request');

                    //
                    //  On this page, BB the page requests json as well as html. But suspiciously, it requests only up to 10 commits
                    //  at the json request. Don't know why. In the result, Tag Details would only load the tags and notes of the
                    //  first 10 commits at all. No tags and notes of subsequent pages. So we have to stick to the html request.
                    //

                    // Tags
                    readCommitIdsFromPullRequestPageResponse(xhr, settings, 'html')
                        .then(loadTagObjectsByCommitIdsJson)
                        .then(insertTagsContainersIntoPullRequestCommitsList)
                        .then(loadTagDetailsByTagNames)
                        .then(setTagDetailsDialogsInCommitsList);
                    // Notes
                    readCommitIdsFromPullRequestPageResponse(xhr, settings, 'html')
                        .then(loadNotesByCommitIdsJson)
                        .then(insertNotesContainersIntoCommitsList)
                        .then(showNotesColumn)
                        .then(setNotesDialogsInCommitsList);
                }
            },
            // Wait:
            // General: At this time, response returned, the browser rendered the tags and registered the tooltips.
            // On commits page: Tag details can be requested in parallel. I assume rendering and registering of tags will be finished until tag details return.
            // On single commit page: Show page / create tag / delete tag: Tag names have to be read from HTML. Wait for finish rendering.
            // At the end of the day, in most use cases calling the above functions have to wait. For convenience, put all in the same setTimeout.
            100
        );
    });

    /**
     * Common functions for tag details and notes.
     */
    function createDialog(triggerElem, dialogId, dialogHtml) {
        //log('createDialog(), dialogId ' + dialogId + ', dialogHtml ' + dialogHtml);
        log('createDialog(), dialogId ' + dialogId);

        // The trigger and the dialog may appended at different times and in any order.
        // But the trigger has to be complete. This means, the attributes 'data-aui-trigger' and 'aria-controls="..."'
        // have to be part of that trigger element at the time of appending to the DOM.
        // The first attempt of implementation (only to add that attributes) didn't work.
        var $triggerElem = $(triggerElem);
        var $triggerElemWithAuiTriggerAttributes = $triggerElem.clone(true);
        $triggerElemWithAuiTriggerAttributes.attr('data-aui-trigger', '');
        $triggerElemWithAuiTriggerAttributes.attr('aria-controls', dialogId);
        $triggerElem.replaceWith($triggerElemWithAuiTriggerAttributes);

        $('body').append('<aui-inline-dialog id="' + dialogId
            + '" responds-to="hover" alignment="left middle" class="tagdetails-aui-inline-dialog">'
            + dialogHtml
            + '</aui-inline-dialog>');

        // The Z-INDEX issue:
        //
        // The inline dialog has it's specific z-index. The JS sets it to 3.000 while displaying.
        // After that (after hide), the z-index is set back to the value the programmer set.
        // Unfortunately, the z-index of the commits tooltips are higher, and are showed in front of tagdetails dialog.
        // We use the inline dialogs event 'aui-show' to force our z-index.
        var $dialog = $('#' + dialogId);
        $dialog.on('aui-show', function () {
            setTimeout(function () {
                // Found out the commits tooltips have a z-Index of 100.000.
                $('#' + dialogId).css('z-index', 200000);
            }, 100);
        });

        // The HIDE-DELAY issue:
        //
        // The dialog has a natural hide-delay of 1000ms and leads to a visibility of about 2000ms. This is too long. Unfortunately,
        // the <aui-inline-dialog> doesn't have the hideDelay parameter as the predecessor had. So we have to hide it manually at
        // mouseleave.
        registerHideDialog($triggerElemWithAuiTriggerAttributes, $dialog);
    }

    function registerHideDialog($triggerElem, $dialog) {

        $($triggerElem).mouseleave(function () {
            setTimeout(function () {
                var $hoverdElem = $(':hover');

                //
                //  Check for both trigger-element and dialog to avoid flickering.
                //  BB could fire a weird event-sequence.
                //
                if ($hoverdElem.filter($triggerElem).length === 0 && $hoverdElem.filter($dialog).length === 0) {
                    $($dialog).attr('open', false);
                }
            }, DIALOG_HIDE_DELAY_MS);
        });

        $($dialog).mouseleave(function () {
            setTimeout(function () {
                var $hoverdElem = $(':hover');

                //
                //  Check for both trigger-element and dialog to avoid flickering.
                //  BB could fire a weird event-sequence.
                //
                if ($hoverdElem.filter($triggerElem).length === 0 && $hoverdElem.filter($dialog).length === 0) {
                    $($dialog).attr('open', false);
                }
            }, DIALOG_HIDE_DELAY_MS);
        });
    }

    function supplementStyles(dialogId) {
        log('supplementStyles(), dialogId ' + dialogId);
        var fullDialogId = '#inline-dialog-' + dialogId;
        // By default the .aui-inline-dialog-contents overflow x and y are 'hidden'. But to show the Y scrollbar as needed, both have to set here.
        AJS.$(fullDialogId + ' .aui-inline-dialog-contents').css({"overflow-x": "auto", "overflow-y": "auto", "max-width": "1000px"});
        // Since BB 5.10.0 on commits page: At hover, BB shows the commit message tooltip as well as the tag tooltip. This looks ugly
        // and is an error in my opinion. In previous BB, hover shows either the commit message tooltip or the tag tooltip.
        // Unfortunately, the commit message tooltip has a higher z-index as the Tag Details ones, to the commit message tooltip may
        // cover it. Files a support ticket: SSP-29130 (JHCD), BSERV-10800.
        // Still in 7.0.1.
        // Workaround:
        // Force Tag Details dialog on top of commit tooltip. Commit tooltip has a z-index of 100.000 (evaluated it Bitbucket 5.10.0).
        AJS.$(fullDialogId).css('z-index', '200000');
    }

    /**
     * Functions for tag details
     */

    function loadTagDetailsByTagNames(tagNames) {
        log('loadTagDetailsByTagNames()');
        log('  tagNames ' + tagNames);
        var url = buildTagdetailsRestBaseUrl() + '/tags?';
        var encodedTagNameUrlParams = [];
        AJS.$.each(tagNames, function (index, value) {
            encodedTagNameUrlParams.push('name=' + encodeURIComponent(value));
        });
        url += encodedTagNameUrlParams.join('&');
        log('  post url ' + url);
        return post = AJS.$.ajax({
            url: url,
            dataType: 'json',
            cache: false
        });
    }

    function loadTagObjectsByCommitIdsJson(commitIdsJsonStr) {
        log('loadTagObjectsByCommitIdsJson()');
        //log('  commitIdsJsonStr ' + commitIdsJsonStr);
        // This is how Bitbucket does it on the commits page.
        // E.g. http://localhost:7990/bitbucket/projects/PROJECT_1/repos/integrationtest/pull-requests/1/commits?start=0&limit=50&contents
        var url = AJS.contextPath() + '/rest/tags/latest/projects/' + gCurrentProjectKey + '/repos/' + gCurrentRepoSlug + '/tags';
        var deferred = $.Deferred();
        log('  post url ' + url);
        // Omitting contentType results in 'XSRF check failed'!
        AJS.$.ajax({
            type: 'POST',
            url: url,
            contentType: "application/json; charset=utf-8",
            data: commitIdsJsonStr,
            dataType: 'json',
            cache: false
        }).done(function (data, textStatus, jqXHR) {
            data.values.length > 0 ? deferred.resolve(data) : deferred.reject();
        }).fail(deferred.reject);
        return deferred;
    }

    // Parameter form ajaxComplete()
    function readTagNamesFromCommitsPageResponse(xhr) {
        log('readTagNamesFromCommitsPageResponse()');
        //log(' xhr ' + JSON.stringify(xhr.responseJSON));
        var tagNames = _.map(xhr.responseJSON.values, function (listElem) {
            return listElem.displayId;
        });
        log('  tagNames ' + tagNames);
        var deferred = $.Deferred();
        tagNames.length > 0 ? deferred.resolve(tagNames) : deferred.reject();
        return deferred;
    }

    function readTagNamesFromCommitViewPageHtml() {
        log('readTagNamesFromCommitViewPageHtml()');
        var tagNames = _.map($('span.tags-info a:not(#tags-see-more)'), function (listElem) {
            var tagName = $(listElem).text();
            return tagName;
        });
        log('  tagNames ' + JSON.stringify(tagNames));
        var deferred = $.Deferred();
        tagNames.length > 0 ? deferred.resolve(tagNames) : deferred.reject();
        return deferred;
    }

    function readCommitIdsFromCommitsPageResponse(xhr, settings) {
        log('readCommitIdsFromCommitsPageResponse()');
        //log('  settings ' + JSON.stringify(settings));
        //log('  xhr ' + JSON.stringify(xhr));
        var commitIdsJsonStr;
        //log('  settings.data ' + settings.data);
        // In case of commits page response the commit ids are in 'data' as json array.
        commitIdsJsonStr = settings.data;
        //log('  commitIdsJsonStr ' +commitIdsJsonStr);
        //log('  commitIdsJsonStr parsed ' +commitIdsJsonStr);
        //log('  commitIdsJsonStr len ' + JSON.parse(commitIdsJsonStr).length);

        var deferred = $.Deferred();
        JSON.parse(commitIdsJsonStr).length > 0 ? deferred.resolve(commitIdsJsonStr) : deferred.reject();
        return deferred;
    }

    function readCommitIdsFromPullRequestPageResponse(xhr, settings, expectedDataType) {
        log('readCommitIdsFromPullRequestPageResponse(), settings.dataType ' + settings.dataType + ', expectedDataType ' + expectedDataType);

        var deferred = $.Deferred();

        if (settings.dataType !== expectedDataType) {
            return deferred.reject();
        }

        var commitIds = [];

        if (settings.dataType === 'json') {
            commitIds = _.map(_.get(xhr, 'responseJSON.values'), function (listElem) {
                return listElem.id;
            });
        } else if (settings.dataType === 'html') {
            commitIds = _.map(AJS.$('tr.commit-row', xhr.responseText), function (listElem) {
                return AJS.$(listElem).data('commitid');
            });
        }

        //log('  settings ' + JSON.stringify(settings));
        //log('  xhr ' + JSON.stringify(xhr));
        log('  commitIdsJsonStr ' + JSON.stringify(commitIds));
        //log('  commitIdsJsonStr parsed ' +commitIdsJsonStr);
        //log('  commitIdsJsonStr len ' + JSON.parse(commitIdsJsonStr).length);

        commitIds.length > 0 ? deferred.resolve(JSON.stringify(commitIds)) : deferred.reject();
        return deferred;
    }

    function insertTagsContainersIntoPullRequestCommitsList(data) {
        log('insertTagsContainersIntoPullRequestCommitsList()');
        var url = AJS.contextPath() + '/projects/' + gCurrentProjectKey + '/repos/' + gCurrentRepoSlug + '/commits?until=';
        //log(' url ' + url);
        var commitIdToTagObjectsMap = {};
        AJS.$.each(data.values, function (index, value) {
            var commitId = value.latestCommit;
            if (!commitIdToTagObjectsMap[commitId]) {
                commitIdToTagObjectsMap[commitId] = [];
            }
            commitIdToTagObjectsMap[commitId].push(value);
        });
        log('  commitIdToTagObjectsMap ' + JSON.stringify(commitIdToTagObjectsMap));
        AJS.$.each(commitIdToTagObjectsMap, function (key, value) {
            var commitId = key;
            var $tr = $('tr td.commit a[href*="' + commitId + '"]').closest('tr');
            var tagObjects = value;
            var $tagContainer = AJS.$(jh.atlassian.bitbucket.tagdetails.panel({
                url: url + encodeURIComponent(tagObjects[0].displayId),
                tags: tagObjects
            }));
            var $messageBody = $tr.find('.message-body');
            $($tagContainer).insertAfter($messageBody);
            $tr.addClass('commit-row has-tags');
        });
        var tagNames = _.map(data.values, function (listElem) {
            return listElem.displayId;
        });
        //log('  tagNames ' + JSON.stringify(tagNames));
        return tagNames;
    }

    function setTagDetailsDialogsInCommitsList(tagNameToTagInfoMap) {
        log('setTagDetailsDialogsInCommitsList()');
        AJS.$('span.tag').each(function (index, element) {
            var $element = AJS.$(element);
            var tagNames = $element.attr('data-names');
            // Tag names are splitted by ':', which is a forbidden character for tag names.
            var tagNameList = tagNames.split(':');
            var firstTagName = tagNameList[0];
            // The commits page may be paged: On scroll down, the browser loads the next n commits and tags ("infinite scroll").
            // The browser will only load the new tags, and this script does it the same way: This script then will only load the new tags.
            // But AJS.$('span.tag') will iterate over all tags received so far. So not all tags the selector gets are in the
            // map. Skip all tags not in the map, because they are already processed.
            if (tagNameToTagInfoMap[firstTagName] == null) {
                return;
            }
            $element.off('mouseenter');  // unbind original tooltip
            var dialogHtml = buildTagDetailsDialogHtml(tagNameList, tagNameToTagInfoMap);
            gDialogIndex += 1;
            var dialogId = 'tagdetails-tag-dialog-' + gDialogIndex;
            createDialog($element, dialogId, dialogHtml);
        });
    }

    function setTagDetailsDialogsOnCommitViewPage(tagNameToTagInfoMap) {
        log('setTagDetailsDialogsOnCommitViewPage()');
        //log('  tagNameToTagInfoMap ' + JSON.stringify(tagNameToTagInfoMap));

        // If there are more than n tags, only the first one is displayed, followed by a clickable pseudo-tag named e.g. '+ 4 more'.
        // That one has a slightly different HTML and - above all - the id="tags-see-more". This is not a tag and must not be processed in
        // the following.
        AJS.$('span.tags-info a:not(#tags-see-more)').each(function (index, element) {
            var $element = $(element);
            var tagName = $element.text();
            //log('  tagName ' + tagName);
            AJS.$('span.tags-info a').parent().die('mouseenter');   // unbind original tooltip. off() doesn't work here.
            var dialogHtml = buildTagDetailsDialogHtml([tagName], tagNameToTagInfoMap); // buildTagDetailsDialogHtml() requires a list.
            gDialogIndex += 1;
            var dialogId = 'bitbucket-tagdetails-tag-dialog-' + gDialogIndex;
            createDialog($element, dialogId, dialogHtml);
        });
    }

    function buildTagDetailsDialogHtml(tagNameList, tagInfoJsonMap) {
        log('buildTagDetailsDialogHtml()');
        //log('  tagNameList ' + tagNameList + ', tagInfoJsonMap ' + JSON.stringify(tagInfoJsonMap));
        var tagsHtml = [];
        // Multiple tags are delimited by ':', e. g. data-names="tag-01:tag-02".
        // In case of >9 tags:
        // data-names="a/b:a]b:anno-2.1:anno-2.2:t10:t11:t6:t7:t8:and more..."
        // 'and more...' of course isn't a tag.
        AJS.$.each(tagNameList, function (index, tagName) {
            var tagInfo = tagInfoJsonMap[tagName];
            var html = '<div class="tagdetails-dialog-tag">';
            html += '<div>' + tagName + '</div>';
            if (tagInfo != null && tagInfo.tagDetails.isAnnotatedTag) {
                var tagDetails = tagInfo.tagDetails;
                html += '<div>' + _.escape(tagDetails.message) + '</div>';
                html += '<table>'
                    + '<tr><td>Tagger:</td><td>'
                    + _.escape(tagDetails.tagger)
                    + '</td></tr>'
                    + '<tr><td>Date:</td><td>'
                    + _.escape(tagDetails.date)
                    + '</td></tr>';
                if (tagDetails.signature) {
                    html += '<tr><td>'
                        + 'Signature:'
                        + ' </td><td><pre>' + _.escape(tagDetails.signature) + '</pre></td></tr>';
                }
                html += '</div>';   // End of .tagdetails-dialog-tag
                html += '</table>';
            }
            tagsHtml.push(html);
        });
        var dialogHtml = '<div class="tagdetails-dialog tagdetails-dialog-tags">';
        dialogHtml += tagsHtml.join('<hr/>');
        dialogHtml += '</div>';
        return dialogHtml;
    }

    /*
     *  N O T E S
     */

    function loadNotesByCommitIdsJson(commitIdsJsonStr) {
        log('loadNotesByCommitIdsJson()');
        //log('  commitIdsJsonStr ' + commitIdsJsonStr);
        var url = buildTagdetailsRestBaseUrl() + '/notes';
        log('  url ' + url);
        var deferred = $.Deferred();
        // Omitting contentType results in 'XSRF check failed'!
        AJS.$.ajax({
            type: 'POST',
            url: url,
            contentType: "application/json; charset=utf-8",
            data: commitIdsJsonStr,
            dataType: 'json',
            cache: false
        }).done(function (data, textStatus, jqXHR) {
            var notesModelList = data;
            notesModelList.length > 0 ? deferred.resolve(notesModelList) : deferred.reject();
        }).fail(deferred.reject);
        return deferred;
    }

    function insertNotesContainersIntoCommitsList(notesModelList) {
        log('insertNotesContainersIntoCommitsList()');
        //log('  notesModelList ' + JSON.stringify(notesModelList));
        AJS.$.each(notesModelList, function (index, element) {
            var notesModel = element;
            var notesCount = _.size(notesModel.namespaceToNotesMap);
            // This container is copied from the comments container. We use the same classes to benefit from the already defined styles.
            var notesContainerHtml = '<span class="comment-count bitbucket-tagdetails-nodes-count">'
                + '<span class="aui-icon aui-icon-small aui-iconfont-page-default"></span>'
                + '<span >' + notesCount + '</span>' // The comments container here uses the class="count". But this class is display:none on pull request page.
                + '</span>';
            var commentCountSelector = 'tr.commit-row[data-commitid=' + notesModel.commitId + '] td.bitbucket-tagdetails-notesColumn';
            AJS.$(commentCountSelector).append(notesContainerHtml);
        });
        return notesModelList;
    }

    function showNotesColumn(notesModelList) {
        log('showNotesColumn()');
        if (notesModelList.length > 0) {
            AJS.$('.bitbucket-tagdetails-notesColumn').show();
            log('  gHaveNotesSeen = true');
            gHaveNotesSeen = true;
        }
        return notesModelList;
    }

    function setNotesDialogsInCommitsList(notesModelList) {
        log('setNotesDialogsInCommitsList()');
        //log('  notesModelList ' + JSON.stringify(notesModelList));
        AJS.$.each(notesModelList, function (index, element) {
            var notesModel = element;
            var notesCountSelector = 'tr.commit-row[data-commitid=' + notesModel.commitId + '] .bitbucket-tagdetails-nodes-count';
            gDialogIndex += 1;
            var dialogId = 'tagdetails-notes-dialog-' + gDialogIndex;
            var dialogHtml = buildNotesDialogHtml(notesModel.namespaceToNotesMap);
            createDialog(notesCountSelector, dialogId, dialogHtml);
        });
    }

    function setNotesLabelOnCommitViewPage(notesModelList) {
        log('setNotesLabelOnCommitViewPage()');
        //log('  notesModelList ' + JSON.stringify(notesModelList));
        if (notesModelList.length > 0) {
            var $notesLabel = AJS.$(jh.atlassian.bitbucket.tagdetails.notesContentOnCommitView({
                notesCount: _.size(notesModelList[0].namespaceToNotesMap)
            }));
        }
        AJS.$('#bitbucket-tagdetails-notesPlaceholderOnCommitView').append($notesLabel);
        return notesModelList;
    }

    function setNotesDialogOnCommitViewPage(notesModelList) {
        log('setNotesDialogOnCommitViewPage()');
        var selector = '.bitbucket-tagdetails-notesLabel>span:nth-child(2)';
        var dialogId = 'bitbucket-tagdetails-notes-dialog';
        var dialogHtml = buildNotesDialogHtml(notesModelList[0].namespaceToNotesMap);
        createDialog(selector, dialogId, dialogHtml);
    }

    function buildNotesDialogHtml(namespaceToNotesMap) {
        log('buildNotesDialogHtml()');
        //log(' namespaceToNotesMap ' + JSON.stringify(namespaceToNotesMap));
        var notes = [];
        AJS.$.each(namespaceToNotesMap, function (key, value) {
            var namespace = key;
            var note = value;
            var noteHtml = '<div class="tagdetails-dialog-note">';
            noteHtml += '<div>' + _.escape(namespace) + '</div>';
            noteHtml += '<div><pre>' + _.escape(note) + '</pre></div>';
            noteHtml += '</div>';
            notes.push(noteHtml);
        });
        var dialogHtml = '<div class="tagdetails-dialog">';
        dialogHtml += notes.join('<br/>');
        dialogHtml += '</div>';
        return dialogHtml;
    }

});

AJS.$(document).ready(function () {
    require('jh/atlassian/bitbucket/tagdetails');
});
